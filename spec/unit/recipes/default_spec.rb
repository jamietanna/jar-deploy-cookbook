#
# Cookbook:: jar-deploy-cookbook
# Spec:: default
#
# Copyright:: 2018, Jamie Tanna, Apache-2.0

require 'spec_helper'

describe 'jar-deploy-cookbook::default' do
  context 'When all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
      end
      runner.converge(described_recipe)
    end

    before :each do
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe).and_raise('include_recipe not matched')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('jar-deploy-cookbook::download_jar')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('java::default')
    end

    it 'installs Java using the java::default recipe' do
      expect_any_instance_of(Chef::Recipe).to receive(:include_recipe).with('java::default')
      chef_run
    end

    it 'creates the user and group' do
      expect(chef_run).to create_group('creates the group')
        .with(group_name: 'jar')

      expect(chef_run).to create_user('creates the user')
        .with(username: 'jar')
        .with(group: 'jar')
    end

    it 'creates the containing directory' do
      expect(chef_run).to create_directory('creates the containing directory')
        .with(path: '/var/jar')
        .with(owner: 'jar')
        .with(group: 'jar')
        .with(recursive: true)
    end

    it 'downloads the JAR using the ::download_jar recipe' do
      expect_any_instance_of(Chef::Recipe).to receive(:include_recipe).with('jar-deploy-cookbook::download_jar')
      chef_run
    end

    it 'creates a systemd service file' do
      expect(chef_run).to create_template('create a systemd service file')
        .with(source: 'unit.service.erb')
        .with(path: '/etc/systemd/system/jar.service')
        .with(owner: 'root')
        .with(group: 'root')
        .with(variables: {
                commandline_arguments: {},
                path_to_jar: '/var/jar/jar.jar',
                user: 'jar'
              })

      expect(chef_run).to(render_file('/etc/systemd/system/jar.service')
        .with_content do |content|
          expect(content).to match %r{^User=jar$}
          expect(content).to match %r{^ExecStart=/usr/bin/java -jar /var/jar/jar\.jar$}
        end)
    end

    it 'enables the service' do
      expect(chef_run).to enable_service('enables the jar service')
        .with(service_name: 'jar')
    end

    it 'starts the service' do
      expect(chef_run).to start_service('starts the jar service')
        .with(service_name: 'jar')
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end

  context 'When all attributes are default, on CentOS 7.4.1708' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'centos', version: '7.4.1708') do |node|
      end

      runner.converge(described_recipe)
    end

    before :each do
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe).and_raise('include_recipe not matched')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('jar-deploy-cookbook::download_jar')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('java::default')
    end

    it 'installs Java using the java::default recipe' do
      expect_any_instance_of(Chef::Recipe).to receive(:include_recipe).with('java::default')
      chef_run
    end

    it 'creates the user and group' do
      expect(chef_run).to create_group('creates the group')
        .with(group_name: 'jar')

      expect(chef_run).to create_user('creates the user')
        .with(username: 'jar')
        .with(group: 'jar')
    end

    it 'creates the containing directory' do
      expect(chef_run).to create_directory('creates the containing directory')
        .with(path: '/var/jar')
        .with(owner: 'jar')
        .with(group: 'jar')
        .with(recursive: true)
    end

    it 'downloads the JAR using the ::download_jar recipe' do
      expect_any_instance_of(Chef::Recipe).to receive(:include_recipe).with('jar-deploy-cookbook::download_jar')
      chef_run
    end

    it 'creates a systemd service file' do
      expect(chef_run).to create_template('create a systemd service file')
        .with(source: 'unit.service.erb')
        .with(path: '/etc/systemd/system/jar.service')
        .with(owner: 'root')
        .with(group: 'root')
        .with(variables: {
                commandline_arguments: {},
                path_to_jar: '/var/jar/jar.jar',
                user: 'jar'
              })

      expect(chef_run).to(render_file('/etc/systemd/system/jar.service')
        .with_content do |content|
          expect(content).to match %r{^User=jar$}
          expect(content).to match %r{^ExecStart=/usr/bin/java -jar /var/jar/jar\.jar$}
        end)
    end

    it 'enables the service' do
      expect(chef_run).to enable_service('enables the jar service')
        .with(service_name: 'jar')
    end

    it 'starts the service' do
      expect(chef_run).to start_service('starts the jar service')
        .with(service_name: 'jar')
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end

  context 'When overriding user and group, when the platform is irrelevant' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['jar']['user'] = 'jar_user'
        node.automatic['jar']['group'] = 'deployment'
      end
      runner.converge(described_recipe)
    end

    before :each do
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe).and_raise('include_recipe not matched')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('jar-deploy-cookbook::download_jar')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('java::default')
    end

    it 'creates the user and group' do
      expect(chef_run).to create_group('creates the group')
        .with(group_name: 'deployment')

      expect(chef_run).to create_user('creates the user')
        .with(username: 'jar_user')
        .with(group: 'deployment')
    end

    it 'creates the containing directory' do
      # notice we're not checking the `path` or `recursive`
      expect(chef_run).to create_directory('creates the containing directory')
        .with(owner: 'jar_user')
        .with(group: 'deployment')
    end

    it 'creates a systemd service file' do
      expect(chef_run).to create_template('create a systemd service file')
        .with(variables: {
                commandline_arguments: {},
                path_to_jar: '/var/jar/jar.jar',
                user: 'jar_user'
              })

      expect(chef_run).to(render_file('/etc/systemd/system/jar.service')
        .with_content do |content|
          expect(content).to match %r{^User=jar_user$}
        end)
    end

  end

  context 'When overriding jar.directory, when the platform is irrelevant' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['jar']['directory'] = '/var/jar-deploy'
      end
      runner.converge(described_recipe)
    end

    before :each do
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe).and_raise('include_recipe not matched')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('jar-deploy-cookbook::download_jar')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('java::default')
    end

    it 'creates the containing directory' do
      # notice we're not checking `user` or `group`
      expect(chef_run).to create_directory('creates the containing directory')
        .with(path: '/var/jar-deploy')
        .with(recursive: true)
    end

    it 'creates a systemd service file' do
      expect(chef_run).to create_template('create a systemd service file')
        .with(variables: {
                commandline_arguments: {},
                path_to_jar: '/var/jar-deploy/jar.jar',
                user: 'jar'
              })

      expect(chef_run).to(render_file('/etc/systemd/system/jar.service')
        .with_content do |content|
          expect(content).to match %r{^ExecStart=/usr/bin/java -jar /var/jar-deploy/jar\.jar$}
        end)
    end
  end

  context 'When overriding jar.configuration.commandline_arguments, when the platform is irrelevant' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['jar']['directory'] = '/var/jar-deploy'
        node.automatic['jar']['configuration']['commandline_arguments']['port'] = 1234
        node.automatic['jar']['configuration']['commandline_arguments']['version'] = nil
      end
      runner.converge(described_recipe)
    end

    before :each do
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe).and_raise('include_recipe not matched')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('jar-deploy-cookbook::download_jar')
      allow_any_instance_of(Chef::Recipe).to receive(:include_recipe)
        .with('java::default')
    end

    it 'creates a systemd service file' do
      expect(chef_run).to create_template('create a systemd service file')
        .with(variables: {
                commandline_arguments: {
                  'port' => 1234,
                  'version' => nil
                },
                path_to_jar: '/var/jar-deploy/jar.jar',
                user: 'jar'
              })

      expect(chef_run).to(render_file('/etc/systemd/system/jar.service')
        .with_content do |content|
          expect(content).to match %r{^ExecStart=/usr/bin/java -jar /var/jar-deploy/jar\.jar --port=1234 --version$}
        end)
    end
  end
end
