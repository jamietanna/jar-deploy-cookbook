require 'spec_helper'

describe 'jar-deploy-cookbook::download_jar' do
  context 'When the jar.jar_uri is not specified, when the platform is irrelevant' do
    let(:runner) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04')
    end

    it 'raises an error' do
      expect { runner.converge(described_recipe) }.to raise_error('jar.jar_uri is not specified')
    end
  end

  context 'When the jar.jar_uri is specified and other attributes are default, when the platform is irrelevant' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['jar']['jar_uri'] = 'https://example.com/jar'
      end
      runner.converge(described_recipe)
    end

    it 'downloads the JAR' do
      expect(chef_run).to create_remote_file('download the JAR')
        .with(source: 'https://example.com/jar')
        .with(path: '/var/jar/jar.jar')
        .with(user: 'jar')
        .with(group: 'jar')
    end
  end

  context 'When the jar.jar_uri is specified and user and group are overriden, when the platform is irrelevant' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['jar']['jar_uri'] = 'https://example.com/jar'
        node.automatic['jar']['user'] = 'abc'
        node.automatic['jar']['group'] = 'def'
      end
      runner.converge(described_recipe)
    end

    it 'downloads the JAR' do
      # notice we don't check for `path`
      expect(chef_run).to create_remote_file('download the JAR')
        .with(source: 'https://example.com/jar')
        .with(user: 'abc')
        .with(group: 'def')
    end
  end

  context 'When the jar.jar_uri is specified and jar.directory is overriden, when the platform is irrelevant' do
    let(:chef_run) do
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04') do |node|
        node.automatic['jar']['jar_uri'] = 'https://example.com/jar'
        node.automatic['jar']['directory'] = '/opt/directory'
      end
      runner.converge(described_recipe)
    end

    it 'downloads the JAR' do
      # notice we don't check `source`, `user` and `group`
      expect(chef_run).to create_remote_file('download the JAR')
        .with(source: 'https://example.com/jar')
        .with(path: '/opt/directory/jar.jar')
    end
  end
end
