#
# Cookbook:: jar-deploy-cookbook
# Recipe:: download_jar
#
# Copyright:: 2018, Jamie Tanna, Apache-2.0
raise 'jar.jar_uri is not specified' if node['jar']['jar_uri'].nil?

remote_file 'download the JAR' do
  source node['jar']['jar_uri']
  path "#{node['jar']['directory']}/jar.jar"
  owner node['jar']['user']
  group node['jar']['group']
end
