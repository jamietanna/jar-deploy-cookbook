#
# Cookbook:: jar-deploy-cookbook
# Recipe:: default
#
# Copyright:: 2018, Jamie Tanna, Apache-2.0
include_recipe 'java::default'

group 'creates the group' do
  group_name node['jar']['group']
end

user 'creates the user' do
  username node['jar']['user']
  group node['jar']['group']
end

directory 'creates the containing directory' do
  path node['jar']['directory']
  owner node['jar']['user']
  group node['jar']['group']
  recursive true
end

include_recipe 'jar-deploy-cookbook::download_jar'

template 'create a systemd service file' do
  source 'unit.service.erb'
  path '/etc/systemd/system/jar.service'
  owner 'root'
  group 'root'
  variables commandline_arguments: node['jar']['configuration']['commandline_arguments'],
            path_to_jar: "#{node['jar']['directory']}/jar.jar",
            user: node['jar']['user']
end

service 'enables the jar service' do
  service_name 'jar'
  action :enable
end

service 'starts the jar service' do
  service_name 'jar'
  action :start
end
