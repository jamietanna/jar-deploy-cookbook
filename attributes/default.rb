node.default['jar']['user'] = 'jar'
node.default['jar']['group'] = 'jar'
node.default['jar']['directory'] = '/var/jar'
node.default['jar']['jar_uri'] = nil
node.default['jar']['configuration']['commandline_arguments'] = {}
