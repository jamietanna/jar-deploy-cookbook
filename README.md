# jar-deploy-cookbook

Companion cookbook for the article [Test-Driven Chef Cookbook Development Using ChefSpec (and a sprinkling of InSpec)](https://www.jvt.me/posts/2018/09/04/tdd-chef-cookbooks-chefspec-inspec/?utm_medium=from_cookbook)
